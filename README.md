# KingFish README

  
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://bitbucket.org/XiaotingLIN/wp_ca4_lin_xiaoting_shi_zehao/src/master/)

  
## Table of Contents

-  [Introduction](#markdown-header-introduction)

-  [Website Description](#markdown-header-website-description)

-  [Features](#markdown-header-features)

-  [How to Install](#markdown-header-how-to-install)

-  [Overview](#markdown-header-overview)

-  [Reference Link](#markdown-header-reference-link)

-  [Video Screencast](#markdown-header-video-screencast)

-  [Remote Website](#markdown-header-remote-website)

  

# Introduction

This website is coding by Zehao Shi and XiaoTing Lin, our team name is KingFish. We will provide a website for users to order and take away food online.   If you have any queries, please send the email to D00198871@student.dkit.ie or D00190152@student.dkit.ie   

# Website Description

The website is allow user to register and login. Each user will have their own accounts. In order to order food, users have to register to their own account, the account database will contain username, password, address, contact number. This will be the convenience of users in the future when they are willing to order food again, they just simply need to click submit to order food. They don't need to enter address, contact number again.
Furthermore, we provide a map for users to navigate the address. In the future, we will add map route and GPS function for users to set starting position and destination. Users will know where they are, the exactly distance they need to travel.   
  
# Features

- A handy Login System, all personal contact details only need to enter once.
- Easy to add, update, delete food by manager
- Easy to place order for users
- Contact page for users to go to the store.   

# Overview

There are some pictures of our website
![](/assets/readme_overview/1.png)
![](/assets/readme_overview/2.png)
![](/assets/readme_overview/3.png)
![](/assets/readme_overview/4.png)
![](/assets/readme_overview/5.png)
![](/assets/readme_overview/6.png)  
![](/assets/readme_overview/7.png) 
![](/assets/readme_overview/8.png) 

# How to Install

##### Attention!   
- For Xampp users
Before opening the website, please go to xampp-phpmyadmin, create a new database named "food", then importing wp_ca4_lin_xiaoting_shi_zehao.sql to database. Backup and drop "food" database if it is already existing.   
If you still can't import food.sql. Using food.txt instead, this txt file will drop all tables and create tables again, but it will initialize all values.

# Reference Link

| Reference Title | Reference Link |

| ------ | ------ |

| Click Handler | https://stackoverflow.com/questions/16589806/javascript-click-handler-not-working-as-expected-inside-a-for-loop |
| GoogleMap API | http://derek.dkit.ie/ |
| xml Http Request | http://www.php.cn/js-tutorial-391587.html |
| bootstrap theme | https://bootswatch.com/3/ |   
| juqery validate | http://www.runoob.com/jquery/jquery-plugin-validate.html |   
  

# Video Screencast

Video screencast address: https://youtu.be/ejC-TMJpiGE  
  
There are some little problems in screencast, but it is fixed in moodle zip folder

# Remote Website

Here is the remote webiste address: https://joyous-river.000webhostapp.com/   

There are some css problems in screencast, but it is fixed in moodle zip folder