<?php return function($req, $res) {

$db = require('lib/database.php');
require_once('models/validation.php');

$validation = [];

$food_name = $req->body('food_name');
$food_type = $req->body('food_type');
$food_company = $req->body('food_company');
$food_description = $req->body('food_description');
$food_price = $req->body('food_price');
$food_image = $req->body('food_image');

$validation = validation::isUpdateFoodValid($validation, $food_name, $food_type, $food_company, $food_description, $food_price);

if(empty($validation)){
    $food_id = $req->body('food_id');
    $food_name = $req->body('food_name');
    $food_type = $req->body('food_type');
    $food_company = $req->body('food_company');
    $food_description = $req->body('food_description');
    $food_price = $req->body('food_price');
    $food_image = $req->body('food_image');

    $statement = $db->prepare('UPDATE food_stocks SET food_name = :food_name, food_type = :food_type, food_company = :food_company, food_description = :food_description, food_price = :food_price, food_image = :food_image WHERE food_id = :food_id');
    $statement->bindValue('food_id', $food_id);
    $statement->bindValue('food_name', $food_name);
    $statement->bindValue('food_type', $food_type);
    $statement->bindValue('food_company', $food_company);
    $statement->bindValue('food_description', $food_description);
    $statement->bindValue('food_price', $food_price);
    $statement->bindValue('food_image', $food_image);
    $statement->execute();
    $foods = $statement->fetchAll();
    $statement->closeCursor();
        
    $res->redirect("/display_food?success-update=1&food_id=$food_id");
}

$res->render('main', 'update_food', [
    'validation' => $validation
]);

} ?>