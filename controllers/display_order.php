<?php return function($req, $res) {

$db = require('lib/database.php');

$query = $db->prepare('SELECT order_id, user_id, delivery_time, total_money, total_quantity FROM orders ORDER BY order_id ASC');
$query->execute();

$orders = $query->fetchAll();
$query->closeCursor();

$res->render('main', 'display_order', [
    'orders' => $orders, 
    'title' => 'Order List'
]);


} ?>