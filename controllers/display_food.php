<?php return function($req, $res) {

$db = require('lib/database.php');

$query = $db->prepare('SELECT food_id, food_name, food_type, food_company, food_description, food_price, food_image FROM food_stocks ORDER BY food_id ASC');
$query->execute();

$foods = $query->fetchAll();
$query->closeCursor();

$res->render('main', 'display_food', [
    'foods' => $foods, 
    'title' => 'Food List'
]);


} ?>