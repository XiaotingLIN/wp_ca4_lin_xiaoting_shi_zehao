<?php return function($req, $res){


$db = require('lib/database.php');

$validation = [];

if(empty($req->body('username'))){
    $validation["username"] = "error" ;
}
if(empty($req->body('password'))){
    $validation["password"] = "error" ;
}


if(empty($validation)){

    $username = $req->body('username');
    $password = $req->body('password');

    $query = $db->prepare("SELECT * FROM users WHERE username = :username");
    $query->execute([$username]);
    if($query->rowCount() > 0 ){
    $row = $query->fetch(PDO::FETCH_OBJ);
    $dbPassword = $row->password;
    $username = $row->username;
    $user_id = $row->user_id;
    if(password_verify($password, $dbPassword)){
        $_SESSION['user_id'] = $user_id;
        $_SESSION['username'] = $username;
        echo json_encode(['status' => 'success']);
    } else {
        echo json_encode(['status' => 'passwordError', 'message' => 'Your Password is wrong']);
    }
    } else {
        echo json_encode(['status' => 'userError', 'message' => 'Your username is wrong']);
    }
    if($username== "user2"&&$password=="123456"){
            $status=1;//success
    }else{
        $status=0;//fail
    }
        echo json_encode($status);//change to json
    
    
    
    $res->redirect("/home?success=1");

}
$res->render('main', 'login', [
    'validation' => $validation
]);

} ?>