<?php return function($req, $res) {
    $db = require('lib/database.php');
    require_once('models/delete_orderdata.php');


    $requestID = $req->query("order_id");
    delete_orderdata::deleteorder($db, $requestID);
   

    $res->redirect("/display_order?success=1&remove_id=$requestID");

} ?>