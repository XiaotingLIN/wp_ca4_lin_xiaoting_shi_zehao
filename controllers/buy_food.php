<?php return function($req, $res) {

$req->sessionSet('user_id', $order_id);

$user_id = $req->session('user_id');

$db = require('lib/database.php');

$query = $db->prepare('SELECT food_id, food_name, food_type, food_company, food_price, food_image FROM food_stocks ORDER BY food_id ASC');
$query->execute();

$foods = $query->fetchAll();
$query->closeCursor();

$res->render('main', 'buy_food', [
    'foods' => $foods, 
    'title' => 'Buy Food',
    'user_id' => $user_id
]);

} ?>