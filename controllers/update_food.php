<?php return function($req, $res) {
    $db = require('lib/database.php');
    $UpdateID = $req->query("food_id");

    $query = $db->prepare('SELECT food_id, food_name, food_type, food_company, food_description, food_price, food_image FROM food_stocks WHERE food_id = ?');
    $query->bindParam(1, $UpdateID, PDO::PARAM_INT);
    $query->execute();

    $food = $query->fetch();
    $query->closeCursor();


    $res->render('main', 'update_food', [
        'update-msg' => $req->query('success-update') === '1',
        'title' => 'Update Food',
        'food_id' => $UpdateID,
        'food_details' => $food
    ]);


} ?>