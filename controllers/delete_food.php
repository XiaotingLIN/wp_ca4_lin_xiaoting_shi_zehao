<?php return function($req, $res) {
    $db = require('lib/database.php');
    require_once('models/delete_data.php');


    $requestID = $req->query("food_id");
    delete_data::deletefood($db, $requestID);

    $res->redirect("/display_food?success=1&remove_id=$requestID");

} ?>