<?php return function($req, $res) {

$db = require('lib/database.php');

require_once('models/validation.php');

$validation = [];

$user_id = $req->body('user_id');
$delivery_time = $req->body('delivery_time');
$total_money_submit = $req->body('total_money_submit');
$total_quantity = $req->body('total_quantity');

$validation = validation::isBuyFoodValid($validation, $delivery_time, $total_quantity);

if(empty($validation)){
    $statement = $db->prepare('INSERT INTO orders(user_id, delivery_time, total_money, total_quantity) VALUES(:user_id, :delivery_time, :total_money, :total_quantity)');
    $statement->bindValue('user_id', $user_id);
    $statement->bindValue('delivery_time', $delivery_time);
    $statement->bindValue('total_money', $total_money_submit);
    $statement->bindValue('total_quantity', $total_quantity);
    $statement->execute();
    $foods = $statement->fetchAll();

    $statement->closeCursor();
        
    $res->redirect("/display_order?success=1");
}

$res->render('main', 'buy_food', [
    'validation' => $validation
]);
} ?>