<?php return function($req, $res){

session_start();

$db = require('lib/database.php');

$validation = [];
$usernameValidation = [];

if(empty($req->body('username'))){
    $validation["username"] = "error" ;
}

if(empty($req->body('password'))){
    $validation["password"] = "error" ;
}
if(empty($req->body('address'))){
    $validation["address"] = "error" ;
}
if(empty($req->body('contact_number'))){
    $validation["contact_number"] = "error" ;
}


if(empty($validation)){
    $username = $req->body('username');
    $password = $req->body('password');
    $address = $req->body('address');
    $contact_number = $req->body('contact_number');



        $query = $db->prepare("INSERT INTO users(username, password, address, contact_number)VALUES(:username,:password, :address,:contact_number)");
        $query->bindValue('username', $username);
        $query->bindValue('password', $password);
        $query->bindValue('address', $address);
        $query->bindValue('contact_number', $contact_number);

        $query->execute();
        $query->fetchAll();
        $query->closeCursor();
        $res->redirect("/login?success=1");
    } 
    $res->render('main', 'register', [
        'usernameValidation' => $usernameValidation
    ]);
} ?>