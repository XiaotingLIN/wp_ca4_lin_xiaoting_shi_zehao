<?php
try{
  

    $config = require('config.php');
    define('APP_BASE_PATH', $config['app_base_url']);

    require_once('lib/Rapid.php');

    $app = new \Rapid\Router();


    $app->GET('/','home');
    $app->GET('/contact','contact');
    $app->GET('/help','help');
    $app->GET('/add_food','add_food');
    $app->GET('/display_food','display_food');
    $app->GET('/update_food','update_food');
    $app->GET('/delete_food','delete_food');
    $app->GET('/buy_food','buy_food');
    $app->GET('/display_order',"display_order");
    $app->GET('/delete_order',"delete_order");
    $app->GET('/login', 'login');
    $app->GET('/register','register');

    $app->POST('/add_food','add_food_process');
    $app->POST('/update_food','update_food_process');
    $app->POST('/buy_food','buy_food_process');
    $app->POST('/register','register_process');
    $app->POST('/login','login_process');


    $app->dispatch();
}
catch(\Rapid\RouteNotFoundException $e){
    $res = $e->getResponseObject();
    $res->status(404);
    $res->render('main', '404', []);
}catch(PDOException $e){
    $res = $e->getResponseObject();
    $res->status(503);
    $res->render('main', '503', []);

} 
catch(Exception $e) {
  http_response_code(500);
exit();
}

?>