<?php

    class delete_data {
        
        private $id;

        public function __construct($args) {
            $this->setID($args['id'] ?? NULL);
        }

        public static function deletefood($pdo, $id){
            $query = $pdo->prepare('DELETE FROM food_stocks WHERE food_id = :food_id');
            $query->bindValue('food_id', $id);
            $query->execute();
            $query->fetchAll();
            $query->closeCursor();
        }
    }




?>