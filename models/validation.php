<?php

    class validation {
        
        private $delivery_time;

        public function __construct($args) {
            $this->setdelivery_time($args['delivery_time'] ?? NULL);
            $this->setdelivery_time($args['total_quantity'] ?? NULL);
        }

        public static function isBuyFoodValid($validation, $delivery_time, $total_quantity){
            if(empty($delivery_time)){
                $validation["delivery_time"] = "Please select the delivery time." ;
            }

            if(empty($total_quantity)){
                $validation["total_quantity"] = "Food Quantity is 0, Please select some food!" ;
            }

            return $validation;
        }

        public static function isAddFoodValid($validation, $food_name, $food_type, $food_company, $food_description, $food_price){
            if(empty($food_name)){
                $validation["food_name"] = "food name can not be empty!" ;
            }
            if(empty($food_type)){
                $validation["food_type"] = "food type can not be empty!" ;
            }
            if(empty($food_company)){
                $validation["food_company"] = "food company can not be empty!" ;
            }
            if(empty($food_description)){
                $validation["food_description"] = "food description can not be empty!" ;
            }
            if(empty($food_price)){
                $validation["food_price"] = "food price can not be empty!" ;
            }
            if(!preg_match('@[A-Za-z0-9]@', $food_name, $matches)){
                $validation["invalid_food_name"] = "invalid food name!" ;
            }
            if(!preg_match('@[A-Za-z0-9]@', $food_type, $matches)){
                $validation["invalid_food_type"] = "invalid food type!" ;
            }
            if(!preg_match('@[A-Za-z0-9]@', $food_company, $matches)){
                $validation["invalid_food_company"] = "invalid food company!" ;
            }
            if(!preg_match('@[A-Za-z0-9]@', $food_description, $matches)){
                $validation["invalid_food_description"] = "invalid food type!" ;
            }

            return $validation;
        }

        public static function isUpdateFoodValid($validation, $food_name, $food_type, $food_company, $food_description, $food_price){
            if(empty($food_name)){
                $validation["food_name"] = "food name can not be empty!" ;
            }
            if(empty($food_type)){
                $validation["food_type"] = "food type can not be empty!" ;
            }
            if(empty($food_company)){
                $validation["food_company"] = "food company can not be empty!" ;
            }
            if(empty($food_description)){
                $validation["food_description"] = "food description can not be empty!" ;
            }
            if(empty($food_price)){
                $validation["food_price"] = "food price can not be empty!" ;
            }
            if(!preg_match('@[A-Za-z0-9]@', $food_name, $matches)){
                $validation["invalid_food_name"] = "invalid food name!" ;
            }
            if(!preg_match('@[A-Za-z0-9]@', $food_type, $matches)){
                $validation["invalid_food_type"] = "invalid food type!" ;
            }
            if(!preg_match('@[A-Za-z0-9]@', $food_company, $matches)){
                $validation["invalid_food_company"] = "invalid food company!" ;
            }
            if(!preg_match('@[A-Za-z0-9]@', $food_description, $matches)){
                $validation["invalid_food_description"] = "invalid food type!" ;
            }

            return $validation;
        }
    }




?>