<?php

    class delete_orderdata {
        
        private $orderid;

        public function __construct($args) {
            $this->setOrderID($args['orderid'] ?? NULL);
        }

        public static function deleteOrder($pdo, $orderid){
            $query = $pdo->prepare('DELETE FROM orders WHERE order_id = :order_id');
            $query->bindValue('order_id', $orderid);
            $query->execute();
            $query->fetchAll();
            $query->closeCursor();
        }
    }




?>