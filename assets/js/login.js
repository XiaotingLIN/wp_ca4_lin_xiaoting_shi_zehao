// function loginStyle(InputUsername, InputID, InputPassword, passwordagain, loginBtn) {

//     var classes = "",oldClasses = "";

//     var username = document.getElementById(InputUsername);

//     var userId = document.getElementById(InputID);

//     var userPass = document.getElementById(InputPassword);

//     var userAgain = document.getElementById(passwordagain);

//     var login = document.getElementById(loginBtn);

//     login.addEventListener("click", function (event) {

//         if (userId.value != "" && userPass.value != "" && username.value != "" && userAgain.value != "" && userPass.value==userAgain.value) {

//             oldClasses = this.getAttribute("class");

//             classes = oldClasses + " disabled";

//             this.setAttribute("class", classes);

//             this.innerHTML = "<i class='icon-refresh icon-spin'></i> Loading...";



//             var nameU = username.value;

//             var idU = userId.value;

//             var passU = userPass.value;



//             event.preventDefault();

//             ajaxLogin(nameU,idU,passU,oldClasses);



//         } else {

//             if (userId.value == "") {

//                 classes = userId.parentNode.getAttribute("class");

//                 userId.parentNode.setAttribute("class", classes + " has-error");

//             }

//             if (userPass.value == "") {

//                 classes = userPass.parentNode.getAttribute("class");

//                 userPass.parentNode.setAttribute("class", classes + " has-error");

//             }

//             if (userAgain.value == "") {

//                 classes = userAgain.parentNode.getAttribute("class");

//                 userAgain.parentNode.setAttribute("class", classes + " has-error");

//             }
//             if (username.value == "") {

//                 classes = username.parentNode.getAttribute("class");

//                 username.parentNode.setAttribute("class", classes + " has-error");

//             }

//         }

//     }, false);

//     var focus = function () {

//         this.parentNode.setAttribute("class", "input-group");

//     };

//     username.addEventListener("focus", focus, false);

//     userId.addEventListener("focus", focus, false);

//     userPass.addEventListener("focus", focus, false);

//     userAgain.addEventListener("focus", focus, false);



// }



// /* Ajax注册 */

// function ajaxLogin(name,pass,address,contact_number,classes){

//     var xmlhttp;



//     if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari

//         xmlhttp = new XMLHttpRequest();

//     } else {// code for IE6, IE5

//         xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

//     }

//     xmlhttp.open("GET", "regAcc.php?username=" + name +  "&password=" + pass+ "&address=" + address + "&contact_number=" + contact_number, false);



//     xmlhttp.onreadystatechange = function () {

//         if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

//             if (xmlhttp.responseText == "1") {

//                 removeElem("form1");

//                 document.getElementById("form-reg").innerHTML = "<h3>Welcome，" + name + "</h3><img class='img-responsive img-circle' src='../pic/default.jpg'><div id='backTo'><a href='../index.html'>Log in!</a></div>";

//             }else{
//                 var login = document.getElementById("login_to");

//                 login.setAttribute("class",classes);

//                 login.innerHTML = "<i class='glyphicon glyphicon-log-in'></i> Register";

//                 removeElem("warningTip");

//                 var tips = "Failed！";

//                 var form = document.getElementById("form-reg");

//                 form.insertBefore(alertBox(tips,"warning"),form.childNodes[0]);

//             }

//         }

//     };

//     xmlhttp.send();

// }



// /* Ajax验证用户名是否存在 */

// function hasUsename(){

//     var xmlhttp;

//     var tips = document.getElementById("sameUsername");

//     var name = document.getElementById("username").value;

//     var patrn = /^.{6,16}$/;



//     if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari

//         xmlhttp = new XMLHttpRequest();

//     } else {// code for IE6, IE5

//         xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

//     }

//     xmlhttp.open("GET", "hasUsername.php?name="+name, false);



//     xmlhttp.onreadystatechange = function () {

//         if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

//             if (xmlhttp.responseText == "1" && patrn.exec(name)) {

//                 tips.style.visibility = "";
//                 tips.style.color = "green";

//                 tips.innerHTML = "* Congratulations! Create account successfully!";

//             }else {

//                 tips.style.visibility = "";

//                 tips.style.color = "red";

//                 tips.innerHTML = "* Account has existed"

//             }

//         }

//     };

//     xmlhttp.send();

// }



// /* 删除指定id的元素 */

// function removeElem(elemId) {

//     if (document.getElementById(elemId)) {

//         var elem = document.getElementById(elemId);

//         elem.parentNode.removeChild(elem);

//     }

// }



// /* 新建提示框innerHTML */

// function alertBox (tip,color){

//     var box = document.createElement("div");

//     box.setAttribute("id","warningTip");

//     box.setAttribute("class","alert alert-"+color+" alert-dismissible");

//     box.setAttribute("role","alert");

//     box.innerHTML = "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><spanaria-hidden='true'>&times;</span></button>"+tip;

//     return box;

// }

// /**

//  * main()

//  */



// //绑定注册框样式事件

// loginStyle("username","password","password-again","login_to");



// //Ajax检查用户名是否重复

// var user_id = document.getElementById("email_address");

// user_id.addEventListener("blur",hasUserName,false);


$(document).ready(function(){
   
    // User login


    $("#login-form").click(function(){
        const username = $("#username").val();
        const password = $("#password").val();
        if(username.value=null){
            $(".username").addClass("is-invalid");
        } else {
            $(".username").removeClass("is-invalid");
        }

        if(password.value){
            $(".password").addClass("is-invalid");
        } else {
            $(".password").removeClass("is-invalid");
        }

        if(username.length != "" && password.length != ""){
            $.ajax({
                type : 'POST',
                url  : 'login.php',
                data : {'username': username, 'password': password},
                dataType : 'JSON',
                success : function(feedback){
                    if(feedback.status === "success"){
                        window.location.href = "";
                    } else if(feedback.status === "passwordError"){
                        $(".password").addClass("is-invalid");
                        $(".passwordError").html(feedback.message);
                        $(".username").removeClass("is-invalid");
                        $(".usernameError").html("");
                    } else if(feedback.status === "usernameError"){
                        $(".password").removeClass("is-invalid");
                        $(".passwordError").html("");
                        $(".username").addClass("is-invalid");
                        $(".usernameError").html(feedback.message);
                    }
                }
            })
        }
    })

})

