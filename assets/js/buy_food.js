function buyDecrease(){
    var total = 0;
    for(var i=0; i<1000; i++) {  
        (function (i) {
           $("#min" + i).click( // https://stackoverflow.com/questions/16589806/javascript-click-handler-not-working-as-expected-inside-a-for-loop
               function () { 
                    if(document.getElementById("quantity" + i).value != null && document.getElementById("quantity" + i).value > 0 ){
                        total = total - parseFloat(document.getElementById('food_price' + i).innerHTML);
                        document.getElementById("total_money").innerHTML = total;
                        document.getElementById("quantity" + i).value--;
                        document.getElementById("total_money_submit").value = document.getElementById("total_money").innerHTML;
                        document.getElementById("total_quantity").value--;
                     }
                    }
           );
           $("#add" + i).click(
               function () { 
                    if(document.getElementById("quantity" + i).value != null){
                        total = total + parseFloat(document.getElementById('food_price' + i).innerHTML);
                        document.getElementById("total_money").innerHTML = total;
                        document.getElementById("quantity" + i).value++;
                        document.getElementById("total_money_submit").value = document.getElementById("total_money").innerHTML;
                        document.getElementById("total_quantity").value++;
               }
                }
           ); 
        })(i);  
   }
}

buyDecrease();
