<h1>Add Food</h1>
<div class="form-group">
<form action='' method='post' enctype="multipart/form-data">
  <label class="col-form-label col-form-label-lg" for="food_name">Food Name</label>
  <input class="form-control form-control-lg" type="text" placeholder="Please Enter Food Name" id='food_name' name='food_name'>
  <?= $locals ['validation']['food_name'] ?>
  <?= $locals ['validation']['invalid_food_name'] ?>
  <br>
  <label class="col-form-label col-form-label-lg" for="food_type">Food Type</label>
  <input class="form-control form-control-lg" type="text" placeholder="Please Enter Food Type" id='food_type' name='food_type'>
  <?= $locals ['validation']['food_type'] ?>
  <?= $locals ['validation']['invalid_food_type'] ?>
  <br>
  <label class="col-form-label col-form-label-lg" for="food_company">Food Company</label>
  <input class="form-control form-control-lg" type="text" placeholder="Please Enter Food Company" id='food_company' name='food_company'>
  <?= $locals ['validation']['food_company'] ?>
  <?= $locals ['validation']['invalid_food_company'] ?>
  <br>
  <label class="col-form-label col-form-label-lg" for="food_description">Food Description</label>
  <input class="form-control form-control-lg" type="text" placeholder="Please Enter Food Description" id='food_description' name='food_description'>
  <?= $locals ['validation']['food_description'] ?>
  <?= $locals ['validation']['invalid_food_description'] ?>
  <br>
  <label class="control-label col-form-label col-form-label-lg" for="food_price">Food Price</label>
  <div class="form-group">
    <div class="input-group mb-3">
      <div class="input-group-prepend">
        <span class="input-group-text">€</span>
      </div>
      <input type="number" class="form-control" aria-label="Amount (to the nearest Euro)" id='food_price' name='food_price' step="any">
      <?= $locals ['validation']['food_price'] ?>
      <div class="input-group-append">
        <span class="input-group-text">.00</span>
      </div>
    </div>
  </div>
    
  <br>
  <label class="col-form-label col-form-label-lg" for="food_image">Food Image</label>
  <input class="form-control form-control-lg" type="file" placeholder="Please upload file" id='food_image' name='food_image'>
  <br>
  <button type="submit" class="btn btn-primary" value="submit">Submit</button>
</form>
</div>
<?php if($locals['success'] === TRUE) { ?>
  <p>Add Food Success!</p>
<?php } ?>