
<div class="col-md-5 mx-auto mt-5">
  
  <div class="card">
    <div class="card-header">
        <h3>Log in</h3>
    </div>
    <form action='' method='post' enctype="multipart/form-data" id='login-form'>
    <div class="card-body">
        <div class="form-group">
            <!--display validation errors here -->
        
            <label for="InputUsername">Username</label>
            <input type="text" id="username" class="form-control is-invalid name" placeholder="Enter your username" required minlength="3"> 
            <div class="invalid-feedback" style="font-size:16px;">Username is required</div>
        </div>

        <div class="form-group">
            <label for="InputPassword">Password</label>
            <input type="password" class="form-control password" id="password" placeholder="Enter Password" maxlength="12" name="password" required minlength="5">
            <small id="passwordHelp" class="form-text text-muted">Password should includes 8 characters.</small>
            <div class="invalid-feedback" style="font-size:16px;">Password is required</div>
        </div>

        <div class="form-group">
        <button type="submit" id="login-submit" class="btn btn-primary btn-info" onclick="login()">Log in</button>   
        </div>
        </form>
    </div>

</div>
<script src="assets/js/login.js"></script>
<script type="text/jscript">

var xmlrequest;
function createXMLHttpRequest(){
      if(window.XMLHttpRequest){
       xmlrequest=new XMLHttpRequest();
      }
      else if(window.ActiveXObject){
         try{
           xmlrequest=new ActiveXObject("Msxm12.XMLHTTP");
         }
         catch(e){
            try{
             xmlrequest=new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch(e){}
         }
       
      }
}
function login(){   
 createXMLHttpRequest();
  var username = document.getElementById("username").value;
  var password = document.getElementById("password").value;
  if(username==""||password==""){
   alert("Please enter username and password");
   return false;
  }
  var url = "check.php?username="+username+"&password="+password;
  xmlrequest.open("POST",url,true);
  xmlrequest.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
   xmlrequest.onreadystatechange = function(){
  if(xmlrequest.readyState == 4){
   if(xmlrequest.status==200){
      var msg = xmlrequest.responseText;   
        if(msg=='1'){
        alert('username or password wrong!');
        user="";
        password="";
        return false;
      } 
      else{       
        window.location.href="<?=APP_BASE_PATH ?>/";
      }
    }
  }
 }
  xmlrequest.send("username="+username+"&password="+password);
 }
</script>
