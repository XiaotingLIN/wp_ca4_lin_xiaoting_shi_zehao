
<div class="card text-white bg-info mb-3">
  <div class="card-header">Buy food</div>
  <div class="card-body">
    <h4 class="card-title">Your favourite Order</h4>
    <form action='' method='post'>
    <?php foreach($locals['foods'] as $foods) { ?>
        <input type="hidden" id='user_id' name='user_id' value='<?= $locals['user_id'] ?>'>
        <p>Food: <?= $foods['food_id'] ?></p>
        <input id="min<?= $foods['food_id'] ?>" name="min" type="button" value="-" />    
        <input id="quantity<?= $foods['food_id'] ?>" name="quantity" type="button" value="0" />  
        <input id="add<?= $foods['food_id'] ?>" name="add" type="button" value="+" />    
        <p>Food Name: <?= $foods['food_name'] ?></p>
        <p>Food Type: <?= $foods['food_type'] ?></p>
        <p>Food Description: <?= $foods['food_description'] ?></p>
        <p>Food Price(€): <label id="food_price<?= $foods['food_id'] ?>"><?= $foods['food_price'] ?></label></p>
        <p>Food Picture: <?= $foods['food_image'] ?></p>
        <br>     
    <?php } ?>
    <p>Total Money: &nbsp;€<label id="total_money" name='total_money'>0</p>
    <input type="hidden" id='total_money_submit' name='total_money_submit' value='0'>
    <input type="hidden" id='total_quantity' name='total_quantity' value='0'>
    <label for="delivery_time">Delivery Time</label>
    <input type="datetime-local" id='delivery_time' name='delivery_time'>
    <?= $locals ['validation']['delivery_time'] ?>
    <!-- <input type="submit" value='submit' class="mb-3 col-4"> -->
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

  </div>
</div>
<script src='assets/js/buy_food.js'></script>
