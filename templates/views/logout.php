<?php
require('lib/database.php');
if(!isset($_SESSION['id'])):
  header("location: login.php");
endif;

?>
   <div class="col-md-12">
   <div class="jumbotron">
  <h1 class="display-3">Hello, <?php echo $_SESSION['username']; ?></h1>
  <p class="lead">Here is tasty take away</p>
  <hr class="my-4">
  <p>Please select your order</p>
  <p class="lead">
    <a class="btn btn-primary btn-lg" href="logout.php" role="button">Logout</a>
  </p>
</div>
   </div>
  