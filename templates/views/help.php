<div class="help-area d-flex align-items-center">
<div class="help-info">
    <h1>Ask for help</h1>
    <h2>Contact us as below</h2>
           
        <div class="single_widget_area mb-5">
            <div class="link">
                <a href="<?=APP_BASE_PATH ?>/contact" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-placement="top" title="Instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-placement="top" title="Pinterest"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                <a href="#" data-toggle="tooltip" data-placement="top" title="Youtube"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</div>
</div>