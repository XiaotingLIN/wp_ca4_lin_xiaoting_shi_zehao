<!-- <?php include('lib/server.php'); ?> -->

<div class="card">
    <div class="card-header">
        <h3>Register</h3>
    </div>
    <form action='' method='post' enctype="multipart/form-data" id='register-form'>
    <div class="card-body">
        <div class="form-group">
            <label for="InputUsername">Username</label>
            <input type="text" id="username" class="form-control is-invalid name" placeholder="Enter your username" name="username" required> 
            <div class="invalid-feedback usernameError" style="font-size:16px;">Username is required</div>
            <span id="user-exists" class="glyphicon form-control-feedback"></span>
            <p id="username-help" class="help-block"></p>

        </div>

        <div class="form-group">
            <label for="InputPassword">Password</label>
            <input type="password" class="form-control password" id="password" placeholder="Enter Password" name="password" onKeyUp="Checkstrength(this.value)">
            <small id="passwordHelp" class="form-text text-muted">Password should includes 8 characters.</small>
            <div class="invalid-feedback" style="font-size:16px;">Password is required</div>

        <div class="form-group">
            <label for="InputPassword2">Password</label>
            <div class="col-md-6 has-feedback" id="password2-box">
            <input type="password" class="form-control password" id="passwordagain" placeholder="Confirm Password" name="password2" id="password2" required>
            <small id="passwordHelp" class="form-text text-muted">Password should includes 8 characters.</small>
            <span id="miss-matching" class="glyphicon form-control-feedback"></span>
            </div>
            <p id="password2-help" class="help-block"></p>
            <div class="invalid-feedback" style="font-size:16px;">Password is required</div>
           
        <div class="form-group">
            <label for="InputAddress">Address</label>
            <input type="text" id="address" class="form-control address" placeholder="Enter your address" value="<?php echo $address; ?>" name="address" required> 
            <span id="miss-matching" class="glyphicon form-control-feedback"></span>
            <div class="invalid-feedback" style="font-size:16px;">Address is required</div>
        </div>

        <div class="form-group">
            <label for="InputTel">Contact Number</label>
            <input type="text" id="contact_number" class="form-control Contact_number" placeholder="Enter Contact Number" name="contact_number" value="<?php echo $contact_number; ?>" required>
            <span id="miss-matching" class="glyphicon form-control-feedback"></span>
            <div class="invalid-feedback" style="font-size:16px;">Contact number is required</div>
        </div>

        <div class="form-group">
        <button type="submit" id="register-submit" class="btn btn-primary btn-info" value="submit">Submit</button>  
        <a href="<?=APP_BASE_PATH ?>/login" style="float:right;margin-top:10px;">Already have an account ?</a>
        </form>
        </div>
    </div>

</div>

<script src="assets/js/register.js"></script>
<script>
$(function () {
    let $form = $("form");
    /*click and declare*/
    $form.on("click", ":input[type!=submit]", function () {
            $(this).prop("value", "");
    });
    /*add click*/
    function addOK(box, span) {
            span.removeClass("glyphicon-remove");
            box.removeClass("has-error");
            span.addClass("glyphicon-ok");
            box.addClass("has-success");
        }

        /*add ×*/
        function addRemove(box, span) {
            span.removeClass("glyphicon-ok");
            box.removeClass("has-success");
            span.addClass("glyphicon-remove");
            box.addClass("has-error");
        }

          /*password validation*/
          $("#password2,#password").blur(function () {
            let password = $("#password").val();
            let password2 = $("#password2").val();
            let $missmatchingSpan = $("#miss-matching");
            let $password2Box = $("#password2-box");
            let $password2Help = $("#password2-help");

            if (password.trim() === "" || password2.trim() === "" || password2 !== password) {
                addRemove($password2Box, $missmatchingSpan);
                $password2Help.html("password can not match");
            } else {
                addOK($password2Box, $missmatchingSpan);
                $password2Help.html("");
            }
        });

        /*ajax validation if passwoword existed*/
        $("#username").blur(function () {
            let username = $("#username").val().trim();
            if (username !== "") {
                $.ajax({
                    url: "login.php",
                    type: "post",
                    data: {
                        username: username
                    },
                    dataType: "text",
                    success: function (data) {
                        let $usernameBox = $("#username-box");
                        let $userExistsSpan = $("#user-exists");
                        let $usernameHelp = $("#username-help");

                        if (data === "exists") {
                            addRemove($usernameBox, $userExistsSpan);
                            $usernameHelp.html("username has existed");
                        } else {
                            addOK($usernameBox, $userExistsSpan);
                            $usernameHelp.html("Username can be used");
                        }
                    },
                    error: function (error) {
                        alert(error);
                    }
                })
            }
        });

})

</script>