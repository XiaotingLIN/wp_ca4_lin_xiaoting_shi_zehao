<?php foreach($locals['orders'] as $orders) { ?>

<table class="table table-hover" class="orders">
  
    <tr class="table-success">
      <th scope="row">Order ID</th>
      <th scope="row">User ID</th>
      <th scope="row">Delivery Time</th>
      <th scope="row">Total Money</th>
      <th scope="row">Total Qunantity</th>
      <th scope="row"></th>
    </tr>

    <tr class="table-info">
        <td><?= $orders['order_id'] ?></td>
        <td><?= $orders['user_id'] ?></td>
        <td><?= $orders['delivery_time'] ?></td>
        <td><?= $orders['total_money'] ?></td>
        <td><?= $orders['total_quantity'] ?></td>
        <td><a href='<?= APP_BASE_PATH ?>/delete_order?order_id=<?= $orders['order_id']?>'><button type="button" class="btn btn-danger">Delete</button></a></td>
    </tr>
      
     
   
  </tbody>
</table> 
<?php } ?>
