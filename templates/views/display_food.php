<h1>My Food List</h1>

<?php if($locals['update-msg'] === TRUE) { ?>
  <p>Update completed!</p>
<?php } ?>
<?php foreach($locals['foods'] as $foods) { ?>
<div class = "foods">
    <table class="table table-hover" class="orders">
    <tr class="table-success">
      <th scope="row">Food Name</th>
      <th scope="row">Food Type</th>
      <th scope="row">Food Company</th>
      <th scope="row">Food Description</th>
      <th scope="row">Food Price(€)</th>
      <th scope="row">Food Picture</th>
      <th scope="row"></th>
      <th scope="row"></th>
    </tr>

    <tr class="table-info">
        <td><?= $foods['food_name'] ?></td>
        <td><?= $foods['food_type'] ?></td>
        <td><?= $foods['food_company'] ?></td>
        <td><?= $foods['food_description'] ?></td>
        <td><?= $foods['food_price'] ?></td>
        <td><?= $foods['food_image'] ?></td>
        <td><a href='<?= APP_BASE_PATH ?>/update_food?food_id=<?= $foods['food_id']?>'><button type="button" class="btn btn-warning">Update</button></a></td>      
        <td><a href='<?= APP_BASE_PATH ?>/delete_food?food_id=<?= $foods['food_id']?>'><button type="button" class="btn btn-danger">Delete</button></a></td>
    </tr>
      
  </tbody>
</table> 
</div>
<?php } ?>



