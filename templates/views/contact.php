<div class="contact-area d-flex align-items-center">
        <div class="contact-info">
            <h2>How to Find Us</h2>
            <p>Contact us by Facebook</p>

            <div class="contact-address mt-50">
                <p><span>address:</span>Ireland</p>
                <p><span>telephone:</span> +888888888</p>
                <p><a href="#">contact@tastytakeaway.com</a></p>
            </div>
        </div>
        <div id="mapDiv">
        <div class="google-map">
            <div id="googleMap"></div>
        </div>
    </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD3GNUNqilg3CdYIedKxEY5zgCl4p7xp-4"></script>
<!-- <script src="js/map-active.js"></script> -->
<script>
    //From http://derek.dkit.ie/
window.onload = onAllAssetsLoaded;
document.write("<div id='loadingMessage'>Loading...</div>");
function onAllAssetsLoaded()
{
    // hide the webpage loading message
    document.getElementById('loadingMessage').style.visibility = "hidden";

    displayMap();
}


function displayMap()
{
    // These constants must start at 0
    // These constants must match the data layout in the 'locations' array below
    let CONTENT = 0; 
    let LATITUDE = 1; 
    let LONGITUDE = 2;
    let locations = [['DkIT', 53.98485693, -6.39410164],
                     ['Dundalk', 54.00428271, -6.40210535],
                     ['Blackrock', 53.96251869, -6.36627104]];

    let dkit_map = new google.maps.Map(document.getElementById('mapDiv'), 
                                     {zoom : 12,
                                      center : new google.maps.LatLng(53.98485693, -6.39410164),
                                      mapTypeId : google.maps.MapTypeId.ROADMAP});

    let location_marker;
    let mapWindow = new google.maps.InfoWindow();

    for (let i = 0; i < locations.length; i++) 
    {  
        location_marker = new google.maps.Marker({position : new google.maps.LatLng(locations[i][LATITUDE], locations[i][LONGITUDE]), map : dkit_map});

        google.maps.event.addListener(location_marker, 'click', (function(location_marker, i) 
                                                     {
                                                         return function() 
                                                         {
                                                             mapWindow.setContent(locations[i][CONTENT]);
                                                             mapWindow.open(dkit_map, location_marker);
                                                         }
                                                     })(location_marker, i));
    }
}
</script>


