<?php include('server.php'); 

    //if user is not logged in, they cannot access this page
    if(empty($_SESSION['username'])){
        header('location: views/login.php');
    }

?>
<div class="jumbotron">
<video autoplay="autoplay" loop="loop" muted>
    <source src="assets/images/video.mp4" type="video/mp4"class="video-bg">
</video>
    <h1 class="display-3">Welcome to Tasty Take Away</h1>
    
    <div class="form-group lead">
      <label for="city-selected">Select your location</label>
      <select class="form-control" id="select">
      <option selected="">Open it and choose your place</option>
        <option>Dundalk</option>
        <option>Dublin</option>
        <option>Drogheda</option>
        <option>Galway</option>
        <option>Cork</option>
      </select>
    </div>
    <hr class="my-4">
    
    <p class="lead">
        <a class="btn btn-primary btn-lg" href="<?=APP_BASE_PATH ?>/buy_food" role="button">Order right now</a>
    </p>
</div>
