<h1>Update Food</h1>

<form action='<?= APP_BASE_PATH ?>/update_food' method='post'>

    <input type="hidden" id='food_id' name='food_id' value='<?= $locals['food_id'] ?>'>
    <br/>
    <label for="food_name">Food Name</label>
    <input type="text" id='food_name' name='food_name' value='<?= $locals['food_details']['food_name'] ?>'>
    <?= $locals ['validation']['food_name'] ?>
    <?= $locals ['validation']['invalid_food_name'] ?>
    <br/><br/>
    <label for="food_type">Food Type</label>
    <input type="text" id='food_type' name='food_type' value='<?= $locals['food_details']['food_type'] ?>'>
    <?= $locals ['validation']['food_type'] ?>
    <?= $locals ['validation']['invalid_food_type'] ?>
    <br/><br/>
    <label for="food_company">Food Company</label>
    <input type="text" id='food_company' name='food_company' value='<?= $locals['food_details']['food_company'] ?>'>
    <?= $locals ['validation']['food_company'] ?>
    <?= $locals ['validation']['invalid_food_company'] ?>
    <br/><br/>
    <label for="food_description">Food Description</label>
    <input type="text" id='food_description' name='food_description' value='<?= $locals['food_details']['food_description'] ?>'>
    <?= $locals ['validation']['food_description'] ?>
    <?= $locals ['validation']['invalid_food_description'] ?>
    <br/><br/>
    <label for="food_price">Food Price</label>
    <input type="number" id='food_price' name='food_price' value='<?= $locals['food_details']['food_price'] ?>'>
    <?= $locals ['validation']['food_price'] ?>
    <br><br>
    <label for="food_image">Food Image</label>
    <input type="file" id='food_image' name='food_image'>

    <input type="submit" value='submit'>
</form>

