
<!DOCTYPE html>
<html lang='en'>
<head>
  <meta charset='UTF-8'>
  <meta name='viewport' content='width=device-width, initial-scale=1.0'>
  <title><?= $locals['title'] ?? 'Tasty Take Away' ?></title>
  <link rel="stylesheet" href="assets/css/main.css">
  <!-- <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
  <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
  <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
  <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/sketchy/bootstrap.min.css" rel="stylesheet" integrity="sha384-N8DsABZCqc1XWbg/bAlIDk7AS/yNzT5fcKzg/TwfmTuUqZhGquVmpb5VvfmLcMzp" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
  <!-- <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/sandstone/bootstrap.min.css" rel="stylesheet" integrity="sha384-G3Fme2BM4boCE9tHx9zHvcxaQoAkksPQa/8oyn1Dzqv7gdcXChereUsXGx6LtbqA" crossorigin="anonymous"> -->
    <link rel="icon" href="assets/images/icon.jpg">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<!-- <script src="http://static.runoob.com/assets/jquery-validation-1.14.0/lib/jquery.js"></script>
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/localization/messages_zh.js"></script> -->
<script type="text/javascript" src="https://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
</head>
<body>
<div class="navbar navbar-expand-lg fixed-top navbar-light bg-light" style="">
      <div class="container">
        <a href='<?=APP_BASE_PATH ?>/' class="navbar-brand">Tasty take away</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="myaccount" aria-expanded="false">My Account <span class="caret"></span></a>
              <div class="dropdown-menu" aria-labelledby="themes">
                <a class="dropdown-item" href="<?=APP_BASE_PATH ?>/display_order">Display Order</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?=APP_BASE_PATH ?>/help">Help</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?=APP_BASE_PATH ?>/contact">Contact</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?=APP_BASE_PATH ?>/buy_food">Buy food</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="download" aria-expanded="false">Management <span class="caret"></span></a>
              <div class="dropdown-menu" aria-labelledby="download">
                <a class="dropdown-item" target="_blank" href="<?=APP_BASE_PATH ?>/add_food">Add Food</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?=APP_BASE_PATH ?>/display_food">Display Food</a>
              </div>
            </li>
          </ul>

          <ul class="nav navbar-nav ml-auto">
            <li class="nav-item">
              <!-- <a class="nav-link" data-target="#register" href="<?=APP_BASE_PATH ?>/">Register</a> -->
              <a href="<?=APP_BASE_PATH ?>/register"><button type="button" data-target="#register" class="btn btn-primary disabled">Register</button></a>
            </li>
          </ul>

          <ul class="nav navbar-nav  my-2 my-lg-0">
            <li class="nav-item">
              <a class="nav-link" href="<?=APP_BASE_PATH ?>/login">
              <button type="button" data-target ="#login" data-toggle="modal" class="btn btn-secondary" onclick="checkLogin()">Log in</button></a>
            </li>
          </ul>

        </div>
      </div>
</div>
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="user_Msg_to" aria-hidden="true">
    <div class="modal-dialog" id="loginBox">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="user_Msg_to">Log in</h4>
            </div>
            <div class="modal-body">
            <div id="inputBox" class="text-center">
                    <p>Log in</p>
                    <form id="login">
                        <p>
                        <div id="username" class="input-group">
                            <span class="input-group-addon"><i class="icon-envelope"></i></span>
                            <input id="InputUsername" class="form-control" type="text" placeholder="username">
                        </div>
                        </p>
                        <p>
                        <div id="password" class="input-group">
                            <span class="input-group-addon"><i class="icon-key"></i></span>
                            <input id="InputPassword" class="form-control" type="password" placeholder="password">
                        </div>
                        </p>
                        <a id="login_to" class="btn btn-block btn-success">
                            <i class="glyphicon glyphicon-log-in"></i> Log in</a>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
            <p>
               Not yet a member? <a href="<?=APP_BASE_PATH ?>/register">Register here</a>
            </p>
                <button type="button" class="btn btn-danger" data-dismiss="modal">
                    Close
                </button>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>

<div class="container" style="margin-top: 80px;">

  <?= \Rapid\Renderer::VIEW_PLACEHOLDER ?>

    <footer id="footer">
        <div class="row" style="text-align: center">
          <div class="col-lg-12">

            <ul class="list-unstyled">
              <li class="float-lg-right"><a href="#top">Back to top</a></li>
                
                <span>|</span> <a href='<?=APP_BASE_PATH ?>/help'>About us </a>

                <span>|</span><a href='<?=APP_BASE_PATH ?>/contact'>Contact us</a>

                <span>|</span>

            </ul>
            <div class="feedback-info">

                <p>
                    <span id="copyYear">2019ca4</span> &copy <?= $locals['db_name'] ?>;
                </p>

            </div>
          </div>
        </div>

    </footer>
</div>

</body>
</html>